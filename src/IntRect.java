import greenfoot.*;

public class IntRect extends IntShape {
    public IntRect(int value, int width, int height) {
        super(value, width, height);

        GreenfootImage bg = this.getImage();
        bg.setColor(this.getBgColor());
        bg.fill();
        this.drawText();
    }

    public void onClick() {
        // do nothing
    }
    
    public String toString() {
        return "[" + Integer.toString(this.value) + "]";
    }
}
