import greenfoot.*;

public class IntBall extends IntShape {
    public IntBall(int value, int radius) {
        super(value, 2* radius, 2 * radius);

        GreenfootImage bg = this.getImage();
        bg.setColor(this.getBgColor());
        bg.fillOval(0, 0, this.width, this.height);
        this.drawText();
    }

    public void onClick() {
        // do nothing
    }

    public String toString() {
        return "(" + Integer.toString(this.value) + ")";
    }
}
