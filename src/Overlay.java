import greenfoot.*;


public class Overlay extends Actor {
    public Overlay(String text, int size, Color fg, Color bg) {
        GreenfootImage img = new GreenfootImage(Game.WORLD_X, Game.WORLD_Y);
        img.setColor(bg);
        img.fill();
        img.fillRect(0, Game.WORLD_Y / 2 - size, Game.WORLD_X, 2 * size);
        Helper.writeCentered(img,
                             text.toUpperCase(),
                             size,
                             fg,
                             Game.WORLD_X / 2,
                             Game.WORLD_Y / 2);
        this.setImage(img);
    }
}
