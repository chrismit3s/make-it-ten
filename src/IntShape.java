import greenfoot.*;

public abstract class IntShape extends Actor {
    protected int value;
    protected int width, height;

    public static final Color[] BG_COLORS = {
            new Color(  0,   0,   0),
            new Color(130, 164,  69),
            new Color(237, 183, 135),
            new Color(236, 152, 167),  // pink
            new Color(192, 222, 129),  // light green
            new Color(240, 115, 159),  // pink
            new Color(205, 230, 188),  // light green
            new Color(100, 107,  99),  // grey
            new Color( 98, 139, 159),  // marine blue
            new Color(195, 171, 125),  // sand
            new Color(207, 111, 125),
            new Color(113,  89,  81),
            new Color(113,  89,  81),
            new Color(113,  89,  81),
            new Color(113,  89,  81)
        };
    public static final Color[] FG_COLORS = {
            new Color(255, 255, 255),
            new Color(255, 255, 255),
            new Color(255, 255, 255),
            new Color(255, 255, 255),
            new Color(130, 164,  69),
            new Color(255, 255, 255),
            new Color(130, 164,  69),
            new Color(255, 255, 255),
            new Color(255, 255, 255),
            new Color(255, 255, 255),
            new Color(255, 255, 255),
            new Color(255, 255, 255),
            new Color(255, 255, 255),
            new Color(255, 255, 255),
            new Color(255, 255, 255)
        };

    public IntShape(int value, int width, int height) {
        this.value = value;
        this.width = width;
        this.height = height;

        // transparent background
        GreenfootImage bg = new GreenfootImage(this.width, this.height);
        bg.setColor(new Color(255, 255, 255, 0));
        bg.fill();
        bg.scale(this.width, this.height);
        this.setImage(bg);
    }

    public int getValue() {
        return this.value;
    }

    public Color getBgColor() {
        return IntShape.BG_COLORS[this.value % IntShape.BG_COLORS.length];
    }

    public Color getFgColor() {
        return IntShape.FG_COLORS[this.value % IntShape.FG_COLORS.length];
    }

    public void act() {
        if (Greenfoot.mouseClicked(this))
            this.onClick();
    }

    public void drawText() {
        GreenfootImage img = this.getImage();
        Helper.writeCentered(img,
            Integer.toString(this.value),
            this.height / 2,
            this.getFgColor(),
            this.width / 2,
            this.height / 2);
        this.setImage(img);
    }

    public abstract void onClick();

    public abstract String toString();

    public int toInteger() {
        return this.value;
    }
}