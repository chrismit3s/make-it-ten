import greenfoot.*;

public class Helper {
    public static void writeCentered(GreenfootImage img, String text, int size, Color color, int x, int y) {
        GreenfootImage textImg = new GreenfootImage(text, size, color, null);  
        x -= textImg.getWidth() / 2;
        y -= textImg.getHeight() / 2;
        img.drawImage(textImg, x, y);
    }

    public static Color colorTransition(Color c1, Color c2, double x) {
        double r, g, b, a;
        r = x * c1.getRed()   + (1 - x) * c2.getRed();
        g = x * c1.getGreen() + (1 - x) * c2.getGreen();
        b = x * c1.getBlue()  + (1 - x) * c2.getBlue();
        a = x * c1.getAlpha() + (1 - x) * c2.getAlpha();
        return new Color((int)r, (int)g, (int)b, (int)a);
    }
}
