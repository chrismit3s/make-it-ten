import greenfoot.*;
import java.lang.Math;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Game extends World {
    private List<IntBall> balls;
    private IntBall center;
    private int level, score, nextLevelIn;
    private Random rng;
    private long prevClick;
    private double angleOffset;

    public static final int BALL_RADIUS = 15, CIRCLE_RADIUS = 120;
    public static final int WORLD_X = 1280 * 3 / 4, WORLD_Y = WORLD_X * 9 / 16;
    public static final int RECT_X = BALL_RADIUS * 6, RECT_Y = BALL_RADIUS * 4;
    public static final int MAX_BALLS = 20, START_BALLS = 10;
    public static final int START_LEVEL = 10;
    public static final Color GREEN_BAR = new Color(150, 214, 105), RED_BAR = new Color(196, 67, 67);
    public static final boolean DEBUG = false;

    public Game() {
        // init helpers, etc
        super(Game.WORLD_X, Game.WORLD_Y, 1);
        this.rng = new Random(System.currentTimeMillis());
        this.prevClick = -1L;
        this.angleOffset = 0.0;        

        // game setup
        this.score = 0;
        this.level = Game.START_LEVEL;
        this.nextLevelIn = this.roundsPerLevel();
        this.balls = new LinkedList<IntBall>();
        while (this.balls.size() != Game.START_BALLS) {
            this.balls.add(0, this.newBall());
            this.merge();

            if (this.balls.get(0).getValue() == this.level)
                this.balls.remove(0);
            else if (this.balls.get(this.balls.size() - 1).getValue() == this.level)
                this.balls.remove(this.balls.size() - 1);
        }
        this.score = 0;  // reset score
        this.center = this.newBall();
        this.draw();
    }

    /**
     * is run everytime the game is started
     */
    public void started() {
        this.removeObjectsOfClass(Overlay.class);
        this.clear();
        this.draw();
    }

    /**
     * is run everytime the game is stopped
     */
    public void stopped() {
        if (!this.isGameOver())
            this.addObject(
                new Overlay(
                    "GAME PAUSED",
                    30,
                    Color.BLACK,
                    new Color(127, 127, 127, 70)),
                Game.WORLD_X / 2,
                Game.WORLD_Y / 2);
    }

    public void act() {
        // on click
        int clickIndex = this.getClickIndex();
        if (clickIndex != -1) {
            // clear screen before changing actors
            this.clear();

            // insert ball
            this.insertCenter(clickIndex);
            

            // merge everything possible
            this.merge();

            // next round
            --this.nextLevelIn;

            // check for level up
            if (this.nextLevelIn == 0) {
                ++this.level;
                this.nextLevelIn = this.roundsPerLevel();
            }

            // draw all changes made
            this.draw();
        }

        // check for game over
        else if (this.isGameOver())
            this.drawGameOver();
            
        // if in debug mode always refresh (for debug output)
        else if (Game.DEBUG) {
            this.clear();
            this.draw();
        }
    }

    private void clear() {
        GreenfootImage bg = this.getBackground();
        bg.setColor(Color.WHITE);
        bg.fill();

        this.removeObject(this.center);

        for (int i = 0; i != this.balls.size(); ++i)
            this.removeObject(this.balls.get(i));
    }

    private void draw() {
        GreenfootImage bg = this.getBackground();

        // debug output
        if (Game.DEBUG) {
            bg.setColor(Color.BLACK);
            //bg.setFont(new Font("noto mono for powerline", 10));
            bg.setFont(new Font("dejavu sans mono", 10));

            MouseInfo m = Greenfoot.getMouseInfo();
            int index = this.getMouseIndex(m);
            int[] pos = this.getMousePos(m);

            bg.drawString(String.format("mouse(%4d|%4d)@%d",
                    pos[0], pos[1],
                    index), 3, Game.WORLD_Y - 5 - Game.BALL_RADIUS);
            bg.drawString(String.format("round(%2d/%2d)",
                    this.nextLevelIn,
                    this.roundsPerLevel()), 3, Game.WORLD_Y - 15 - Game.BALL_RADIUS);
            bg.drawString(String.format("angleOffset[%+06.1f]",
                    this.angleOffset * 180.0 / Math.PI), 3, Game.WORLD_Y - 25 - Game.BALL_RADIUS);
            bg.drawString(String.format("#balls(%2d/%2d)",
                    this.balls.size(),
                    Game.MAX_BALLS), 3, Game.WORLD_Y - 35 - Game.BALL_RADIUS);
        }

        // level rect in the top left corner
        bg.setColor(IntBall.BG_COLORS[this.level]);
        bg.fillRect(0, 0, Game.RECT_X, Game.RECT_Y);
        Helper.writeCentered(bg,
            Integer.toString(this.level),
            Game.RECT_Y / 2,
            IntBall.FG_COLORS[this.level],
            Game.RECT_X / 2,
            Game.RECT_Y / 2);

        // score rect in the top right corner
        bg.setColor(IntBall.BG_COLORS[this.level]);
        bg.fillRect(Game.WORLD_X - Game.RECT_X, 0, Game.RECT_X, Game.RECT_Y);
        Helper.writeCentered(
            bg,
            Integer.toString(this.score),
            Game.RECT_Y / 2,
            IntBall.FG_COLORS[this.level],
            Game.WORLD_X - Game.RECT_X / 2,
            Game.RECT_Y / 2);

        // level progress bar
        int progressWidth = (Game.WORLD_X - 2 * Game.RECT_X) * this.nextLevelIn / this.roundsPerLevel();
        bg.setColor(Game.GREEN_BAR);
        bg.fillRect(Game.WORLD_X - Game.RECT_X - progressWidth, 0, progressWidth, Game.BALL_RADIUS);
        
        // failure bar
        double frac = (double)this.balls.size() / Game.MAX_BALLS;
        int failWidth = (int)(Game.WORLD_X * frac);
        bg.setColor(Helper.colorTransition(Game.RED_BAR, Game.GREEN_BAR, frac));
        bg.fillRect(Game.WORLD_X - failWidth, Game.WORLD_Y - Game.BALL_RADIUS, failWidth, Game.BALL_RADIUS);

        // draw center
        this.addObject(this.center, Game.WORLD_X / 2, Game.WORLD_Y / 2);

        // draw cirlce
        double x, y, angle;
        double angleStep = this.getAngleStep();
        for (int i = 0; i != this.balls.size(); ++i) {
            angle = this.angleOffset + angleStep * i;
            x = Game.WORLD_X / 2 + Game.CIRCLE_RADIUS * Math.cos(angle);
            y = Game.WORLD_Y / 2 - Game.CIRCLE_RADIUS * Math.sin(angle);

            if (Game.DEBUG && i == 0) {
                bg.setColor(new Color(200, 0, 0, 200));
                bg.fillOval(
                    (int)x - Game.BALL_RADIUS * 6 / 5,
                    (int)y - Game.BALL_RADIUS * 6 / 5,
                    Game.BALL_RADIUS * 12 / 5,
                    Game.BALL_RADIUS * 12 / 5);
            }

            this.addObject(this.balls.get(i), (int)x, (int)y);
        }
    }

    private int roundsPerLevel() {
        return 3 + (this.level - 3) * (this.level - 4) / 2;
    }

    private void insertCenter(int index) {
        if (index < 0)
            return;

        // angle the new ball position makes with the positive x-axis
        // see notes: 2019-11-28-make-it-ten.git
        double angle = this.getAngleStep() * (index - 0.5);

        // insert center and create a new one
        this.balls.add(index, this.center);
        this.center = this.newBall();

        // calculate new offset angle so that the newly inserted ball doesnt jump around
        this.angleOffset = angle - this.getAngleStep() * index;  // also notice that the angle step changes with the insertion so this cant be simplified
    }

    private void merge() {
        // check if starting from ball i, one can get exactly this.level as a sum
        // runs in O(n^2)... :(
        int sum, offset, val;
        for (int i = 0; i < this.balls.size(); ++i) {
            // check if it sums to this.level
            for (sum = 0, offset = 0; offset < this.balls.size(); ++offset) {
                val = this.balls.get((i + offset) % this.balls.size()).getValue();
                if (val == this.level)
                    break;

                sum += val;
                if (sum >= this.level)
                    break;
            }

            // remove the balls and create the new one
            if (sum == this.level) {
                this.balls.add(i, this.newBall(this.level));
                this.removeBalls(i + 1, offset + 1);  // remove after the inserted ball

                // update score
                this.score += offset * this.level;
            }
        }

        // remove consecutive balls of value this.level (when theres more than 4 of them)
        // runs in O(n)
        int consecBalls = 0;
        for (int i = 0; i < 2 * this.balls.size(); ++i) {
            if (this.balls.get(i % this.balls.size()).getValue() == this.level)
                ++consecBalls;
            else {
                if (consecBalls >= 5) {
                    this.removeBalls(i - consecBalls, consecBalls);

                    // update score
                    this.score += (consecBalls - 4) * 100 + ((this.level > 10) ? this.level : 0);
                }
                consecBalls = 0;
            }
        }
    }

    private boolean isGameOver() {
        return this.balls.size() >= Game.MAX_BALLS;
    }

    private void drawGameOver() {
        this.addObject(
            new Overlay(
                "GAME OVER",
                30,
                Color.RED,
                new Color(0, 0, 0, 100)),
            Game.WORLD_X / 2,
            Game.WORLD_Y / 2);
        Greenfoot.stop();
    }

    private void removeObjectsOfClass(Class clazz) {
        for (Actor a : (Actor[])this.getObjects(clazz).toArray(new Actor[0]))
            this.removeObject(a);
    }

    private void removeBalls(int start, int count) {
        // handle negative indices
        while (start < 0)
            start += this.balls.size();

        // remove till end
        for (; count > 0 && start != this.balls.size(); --count)
            this.balls.remove(start);

        // wrap around
        for (; count > 0; --count)
            this.balls.remove(0);
    }

    private IntBall newBall() {
        return this.newBall(rng.nextInt(this.level - 1) + 1);
    }

    private IntBall newBall(int value) {
        return new IntBall(value, Game.BALL_RADIUS);
    }

    private double getAngleStep() {
        return 2 * Math.PI / this.balls.size();
    }

    private int[] getMousePos(MouseInfo m) {
        return (m == null) ? new int[]{0, 0} : new int[]{m.getX() - Game.WORLD_X / 2, Game.WORLD_Y / 2 - m.getY()};
    }

    private int getMouseIndex(MouseInfo m) {
        int[] pos = getMousePos(m);

        // get clicked angle and calculate index
        double angle = Math.atan2(pos[1], pos[0]) - this.angleOffset;

        // map to [0, PI)
        if (angle < 0.0)
            angle += 2 * Math.PI;

        return (int)(angle / this.getAngleStep() + 1);
    }

    private int getClickIndex() {
        // check for mouse left click, with a 300 ms cooldown
        MouseInfo m = Greenfoot.getMouseInfo();
        if (m == null || m.getButton() != 1 || System.currentTimeMillis() - this.prevClick < 300L)
            return -1;
        this.prevClick = System.currentTimeMillis();

        // get clicked angle and angle step between balls
        return this.getMouseIndex(m);
    }
}
